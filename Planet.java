package spacecolonies;

import java.util.Arrays;

public class Planet implements Comparable<Planet> {
    private String name;
    private Skills minSkills;
    private Person[] population;
    private int populationSize;
    private int capacity;


    public Planet(
        String planetName,
        int planetAgri,
        int planetMedi,
        int planetTech,
        int planetCap) {

        name = planetName;
        minSkills = new Skills(planetAgri, planetMedi, planetTech);
        capacity = planetCap;
        population = new Person[capacity];
        populationSize = 0;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }


    public Skills getSkills() {
        return minSkills;
    }


    /**
     * @return population
     */

    public Person[] getPopulation() {
        return population;
    }


    /**
     * @return capacity
     */

    public int getCapacity() {
        return capacity;
    }


    /**
     * @return the number of available places left in planet
     */
    public int getAvailability() {
        return capacity - populationSize;
    }


    /**
     * @return true if population has reached max capacity, false otherwise
     */
    public boolean isFull() {
        return populationSize == capacity;
    }


    public boolean addPerson(Person newbie) {
        // checks that the planet isn't full and that the newbie is qualified
        if (!isFull() && isQualified(newbie)) {
            population[populationSize] = newbie;
            populationSize++;
            return true;
        }
        return false;
    }


    /**
     * @param applicant
     * @return true if applicant skills are above the minimum, false otherwise
     */
    public boolean isQualified(Person applicant) {
        Skills applicantSkills = applicant.getSkills();
        if (minSkills.isBelow(applicantSkills)) {
            return true;
        }
        else {
            return false;
        }
    }


    @Override
    public String toString() {
        int agri = minSkills.getAgriculture();
        int medi = minSkills.getMedicine();
        int tech = minSkills.getTechnology();

        StringBuilder sb = new StringBuilder(name + ", population"
            + populationSize);
        sb.append(" (cap: " + capacity + "), Requires: ");
        sb.append("A >= " + agri + ", M >= " + medi + ", T >= " + tech);
        return sb.toString();
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj == null)
            return false;
        if (this.getClass() == obj.getClass()) {
            Planet other = (Planet)obj;

            return name.equals(other.name) && minSkills.equals(other.minSkills)
                && Arrays.equals(population, other.population)
                && populationSize == other.populationSize
                && capacity == other.capacity;
        }
        else {
            return false;
        }
    }


    @Override
    public int compareTo(Planet other) {
        if (this.getAvailability() > other.getAvailability()) {
            return 1;
        }
        else if (this.getAvailability() < other.getAvailability()) {
            return -1;
        }
        else {
            return 0;
        }
    }
    
}
