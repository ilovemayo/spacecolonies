package spacecolonies;

import java.util.Arrays;
import list.AList;

public class ColonyCalculator {
    /**
     * field variables
     */
    public static int NUM_PLANETS = 3;
    public static int MIN_SKILL_LEVEL = 1;
    public static int MAX_SKILL_LEVEL = 5;
    private ArrayQueue<Person> applicantQueue;
    private AList<Person> rejectBus;
    private static Planet[] planets = new Planet[NUM_PLANETS + 1];


    /**
     * constructor
     * 
     * @param applicantQueue
     * @param planets
     */
    public ColonyCalculator(
        ArrayQueue<Person> applicantQueue,
        Planet[] planets) {

        this.applicantQueue = applicantQueue;
        ColonyCalculator.planets = planets;

        rejectBus = new AList<>();
    }


    /**
     * @return the applicant queue
     */
    public ArrayQueue<Person> getQueue() {
        return applicantQueue;
    }


    /**
     * @return planets
     */
    public static Planet[] getPlanets() {
        return planets;
    }


    public Planet getPlanetForPerson(Person nextPerson) {
        // make sure queue is not empty
        if (!applicantQueue.isEmpty()) {
            // make sure planet is not full
            Planet preferredPlanet = planets[nextPerson.getPlanet()];
            if (nextPerson.getPlanet() == 0) {
                return getMostAvailablePlanet(nextPerson);
            }
            if (!preferredPlanet.isFull() && preferredPlanet.isQualified(
                nextPerson)) {
                return preferredPlanet;
            }
        }
        return null;
    }


    private Planet getPreferredPlanet(Person person, int planet) {
        // TODO might not need this
        return planets[0];
    }


    private Planet getMostAvailablePlanet(Person person) {
        Planet[] planetsCopy = planets;
        Arrays.sort(planetsCopy);
        return planetsCopy[0];
    }


    public boolean accept() {
        // make sure queue is not empty
        if (!applicantQueue.isEmpty()) {
            Person nextPerson = applicantQueue.getFront();
            Planet planet = getPlanetForPerson(nextPerson);

            int planetIndex = Arrays.asList(planets).indexOf(planet);
            // saves from null pointer
            if (planetIndex == 0) {
                return false;
            }
            planets[planetIndex].addPerson(applicantQueue.dequeue());
            return true;
        }
        return false;
    }


    public void reject() {
        // TODO
        rejectBus.add(applicantQueue.dequeue());
    }


    public Planet planetByNumber(int planet) {
        if (1 <= planet && planet <= 3) {
            return planets[planet];
        }
        else {
            return null;
        }
    }
}
