package spacecolonies;

public class SpaceColonyDataException extends Exception {
    public SpaceColonyDataException() {
    }


    public SpaceColonyDataException(String message) {
        super(message);
    }
}
