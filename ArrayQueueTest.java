package spacecolonies;

public class ArrayQueueTest extends student.TestCase { 
    private ArrayQueue<String> queue1;
    
    public void setUp() {
        queue1 = new ArrayQueue<>();
    }
    
    public void testQueue() {
        queue1.enqueue("bye");
        queue1.enqueue("hi");
        queue1.enqueue("bye");
        queue1.enqueue("hi");
        queue1.enqueue("bye");
        queue1.enqueue("hi");
        queue1.enqueue("bye");
        queue1.enqueue("hi");
        queue1.enqueue("bye");
        queue1.enqueue("hi");
        queue1.enqueue("bye");
        queue1.enqueue("hi");
        queue1.enqueue("bye");
        assertEquals("bye", queue1.dequeue());
        assertEquals("hi", queue1.getFront());
        
    }
}
