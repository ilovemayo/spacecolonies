package spacecolonies;

import queue.EmptyQueueException;
import queue.QueueInterface;

public class ArrayQueue<T> implements QueueInterface<T> {
    private T[] queue;
    private int enqueueIndex;
    private int dequeueIndex;
    private int size;

    private static final int DEFAULT_CAPACITY = 10;
    public static final int MAX_CAPACITY = 100;


    /**
     * default constructor
     */
    public ArrayQueue() {
        this(DEFAULT_CAPACITY);
    }


    /**
     * overloaded constructor
     */
    public ArrayQueue(int initialCapacity) {
        // safe cast to generic array because new array contains null entries
        @SuppressWarnings("unchecked")
        T[] tempQueue = (T[])new Object[initialCapacity + 1];
        queue = tempQueue;

        // sets enqueueIndex to end of array and dequeueIndex/size to 0
        enqueueIndex = queue.length - 1;
        dequeueIndex = size = 0;
    }


    /**
     * @return capacity of queue
     */
    public int getLength() {
        return queue.length;
    }


    /**
     * @return number of items in queue
     */
    public int getSize() {
        return size;
    }


    /**
     * @return true if array is empty, false otherwise
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }


    /**
     * @return true if array is full, false otherwise
     */
    private boolean isFull() {
        // accounts for unused location
        return size == queue.length - 1;
    }


    /**
     * enqueues a new entry
     */
    @Override
    public void enqueue(T newEntry) {
        ensureCapacity();
        enqueueIndex = (enqueueIndex + 1) % queue.length;
        queue[enqueueIndex] = newEntry;
        size++;
    }


    /**
     * doubles the capacity if it's full
     */
    private void ensureCapacity() {
        if (dequeueIndex == ((enqueueIndex + 2) % queue.length)) {
            T[] oldQueue = queue;
            int oldSize = oldQueue.length;
            int newSize = 2 * oldSize;
            // checkCapacity(newSize);

            @SuppressWarnings("unchecked")
            T[] tempQueue = (T[])new Object[newSize];
            queue = tempQueue;
            for (int i = 0; i < oldSize - 1; i++) {
                queue[i] = oldQueue[dequeueIndex];
                dequeueIndex = (dequeueIndex + 1) % oldSize;
            }
            dequeueIndex = 0;
            enqueueIndex = oldSize - 2;
        }
    }


    /**
     * dequeues front entry
     */
    @Override
    public T dequeue() {
        if (isEmpty())
            throw new EmptyQueueException();
        else {
            T front = queue[dequeueIndex];
            queue[dequeueIndex] = null;
            dequeueIndex = (dequeueIndex + 1) % queue.length;
            size--;
            return front;
        }
    }


    @Override
    public T getFront() {
        if (isEmpty())
            throw new EmptyQueueException();
        else
            return queue[dequeueIndex];
    }


    @Override
    public void clear() {
        // TODO
    }


    private int incrementIndex(int index) {
        return ((index + 1) % queue.length);
    }


    public T[] toArray() {
        return queue;
    }


    public String toString() {
        return "";
        // TODO
    }


    public boolean equals(Object obj) {
        return false;
        // TODO
    }
}
