package spacecolonies;

public class PersonTest extends student.TestCase {
    private Person person1;
    private Person person2;


    public void setUp() {
        person1 = new Person("David Weisiger", 1, 2, 3, 4);
        person2 = new Person("Jeff Oh", 4, 3, 2, 1);
    }


    public void testToString() {
        assertEquals("No-Planet David Weisiger A:1 M:2 T:3", person1
            .toString());
        // assertEquals("Jeff Oh A:4 M:3 T:2 Planet1", person2.toString());
    }
}
