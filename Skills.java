package spacecolonies;

/**
 * skills class
 *
 * @author dbw
 * @version 11.06.17
 */
public class Skills {
    /**
     * field variables
     */
    private int agriculture;
    private int medicine;
    private int technology;


    /**
     * constructor
     *
     * @param ag
     * @param med
     * @param tech
     */
    public Skills(int ag, int med, int tech) {
        agriculture = ag;
        medicine = med;
        technology = tech;
    }


    /**
     * @return agriculture
     */
    public int getAgriculture() {
        return agriculture;
    }


    /**
     * @return medicine
     */
    public int getMedicine() {
        return medicine;
    }


    /**
     * @return technology
     */
    public int getTechnology() {
        return technology;
    }


    /**
     * @param other
     * @return true if all three skills are less than those in other, false
     *         otherwise
     */
    public boolean isBelow(Skills other) {
        return agriculture <= other.agriculture && medicine <= other.medicine
            && technology <= other.technology;
    }


    /**
     * @param obj
     * @return true if all skills are equal, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj == null)
            return false;
        if (this.getClass() == obj.getClass()) {
            Skills other = (Skills)obj;

            return agriculture == other.agriculture
                && medicine == other.medicine && technology == other.technology;
        }
        else {
            return false;
        }
    }


    /**
     * @return string representation of skills
     */
    @Override
    public String toString() {
        return "A:" + agriculture + " M:" + medicine + " T:" + technology;
    }
}
