package spacecolonies;

import java.io.FileNotFoundException;
import bsh.ParseException;

public class ProjectRunner {

    public static void main(String[] args)
        throws FileNotFoundException,
        ParseException,
        SpaceColonyDataException {
        @SuppressWarnings("unused")
        ColonyReader colonyReader;
        
        System.out.println("test");

        if (args.length == 2)
            colonyReader = new ColonyReader(args[0], args[1]);
        else
            colonyReader = new ColonyReader("input.txt", "planets.txt");

    }
}
