package spacecolonies;

import java.awt.Color;
import CS2114.Button;
import CS2114.CircleShape;
import CS2114.Shape;
import CS2114.TextShape;
import CS2114.Window;
import CS2114.WindowSide;
import list.AList;

public class SpaceWindow {
    /**
     * field variables
     */
    private Window window;
    private ColonyCalculator colonyCalculator;
    
    private Button accept;
    private Button reject;
    private AList<CircleShape> personCircles;
    private static int DISPLAY_FACTOR;
    private static int PLANET_HEIGHT;
    private static int PLANET_SIZE;
    private static int QUEUE_STARTX = 100;
    private static int QUEUE_STARTY = 100;
    private static int CIRCLE_SIZE = 50;
    private boolean justAccepted;
    private TextShape errorMessage;
    private Shape[] planetShapes;
    private static Color[] PLANET_COLORS = { Color.CYAN, Color.LIGHT_GRAY,
        Color.PINK, Color.YELLOW };

    private TextShape applicant;
    private ArrayQueue<CircleShape> circleQueue;


    public SpaceWindow(ColonyCalculator colonyCalculator) {
        this.colonyCalculator = colonyCalculator;

        window = new Window("Space Colony Placement");

        // adds applicants to top left of window with some padding
        applicant = new TextShape(5, 5, colonyCalculator.getQueue().getFront()
            .toString());
        applicant.setBackgroundColor(Color.WHITE);
        window.addShape(applicant);

        circleQueue = new ArrayQueue<>();
        updatePlanetQueue();

        // adds accept button to bottom of window
        accept = new Button("ACCEPT");
        accept.onClick(this, "clickedAccept");
        window.addButton(accept, WindowSide.SOUTH);

        // adds reject button to bottom of window
        reject = new Button("REJECT");
        reject.onClick(this, "clickedReject");
        window.addButton(reject, WindowSide.SOUTH);

    }


    public void updateApplicantQueue() {
        window.removeShape(applicant);
        applicant = new TextShape(5, 5, colonyCalculator.getQueue().getFront()
            .toString());
        applicant.setBackgroundColor(Color.WHITE);
        window.addShape(applicant);
        accept.enable();
    }


    public void updatePlanetQueue() {
        ArrayQueue<Person> personQueue = new ArrayQueue<>();
        personQueue = colonyCalculator.getQueue();

        ArrayQueue<CircleShape> temp = circleQueue;

        int count = 0;
        while (!personQueue.isEmpty()) {
            int planetNumber = personQueue.dequeue().getPlanet();
            CircleShape newCircle = new CircleShape(QUEUE_STARTX + CIRCLE_SIZE
                * count, QUEUE_STARTY, CIRCLE_SIZE,
                PLANET_COLORS[planetNumber]);
            temp.enqueue(newCircle);
            count++;
        }
        for (int i = 0; i < 8; i++) {
            window.addShape(temp.dequeue());
        }
    }


    public void clickedAccept(Button button) {
        if (colonyCalculator.accept()) {
            updateApplicantQueue();

            circleQueue.dequeue();
            updatePlanetQueue();
        }
        else {
            accept.disable();
        }
    }


    public void clickedReject(Button button) {
        colonyCalculator.reject();
        updateApplicantQueue();

        circleQueue.dequeue();
        updatePlanetQueue();
    }
}
