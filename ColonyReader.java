package spacecolonies;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import bsh.ParseException;

/**
 * TODO main problem here is handling the index 0 case in planets, and also
 * statically referencing the planets array, do we have to call readPlanetFile
 * before readQueueFile?
 * 
 * @author davidweisiger
 *
 */
public class ColonyReader {
    private Planet[] planets;
    private ArrayQueue<Person> queue;


    public ColonyReader(String applicantFileName, String planetFileName)
        throws FileNotFoundException,
        ParseException,
        SpaceColonyDataException {

        planets = readPlanetFile(planetFileName);
        queue = readQueueFile(applicantFileName);

        new SpaceWindow(new ColonyCalculator(queue, planets));
    }


    public Planet[] readPlanetFile(String filename)
        throws FileNotFoundException,
        ParseException,
        SpaceColonyDataException {
        Scanner file = new Scanner(new File(filename));

        planets = new Planet[4];
        int numPlanets = 0;

        // read file while it has lines and haven't reached 3 planets
        while (file.hasNextLine() && numPlanets < 3) {
            // split the line by comma and whitespace to make planets object
            String[] split = file.nextLine().split(", *");

            // make sure that there are 5 comma separated values
            if (split.length == 5) {
                String name = split[0];
                int agri = Integer.valueOf(split[1]);
                int medi = Integer.valueOf(split[2]);
                int tech = Integer.valueOf(split[3]);
                int cap = Integer.valueOf(split[4]);

                // check that skills are between 1 and 5
                if (isInSkillRange(agri, medi, tech)) {
                    Planet planet = new Planet(name, agri, medi, tech, cap);
                    planets[numPlanets + 1] = planet;
                    numPlanets++;
                }
                // otherwise throw an scd exception
                else {
                    throw new SpaceColonyDataException();
                }
            }
            // otherwise throw a parse exception
            else {
                throw new ParseException();
            }
        }

        // if there are less than three planets throw an exception
        if (numPlanets < 3) {
            throw new SpaceColonyDataException();
        }
        else {
            return planets;
        }
    }


    public ArrayQueue<Person> readQueueFile(String fileName)
        throws FileNotFoundException,
        SpaceColonyDataException {
        ArrayQueue<Person> personQueue = new ArrayQueue<>();
        Scanner file = new Scanner(new File(fileName));

        int count = 0;
        while (file.hasNextLine()) {
            String[] split = file.nextLine().split(", *");
            if (split.length == 5) {
                String name = split[0];
                int agri = Integer.valueOf(split[1]);
                int medi = Integer.valueOf(split[2]);
                int tech = Integer.valueOf(split[3]);
                int planetPreference = getPlanetNumber(split[4]);

                // check that skills are between 1 and 5
                if (isInSkillRange(agri, medi, tech)) {
                    Person newPerson = new Person(name, agri, medi, tech,
                        planetPreference);
                    personQueue.enqueue(newPerson);
                }
                // otherwise throw an scd exception
                else {
                    throw new SpaceColonyDataException();
                }
            }
            count++;
        }
        return personQueue;
    }


    private int getPlanetNumber(String str) {
        // Planet[] planetsArray = ColonyCalculator.getPlanets();
        String[] planetNames = new String[planets.length];

        // creates an array of planet names
        for (int i = 1; i < planets.length; i++) {
            planetNames[i] = planets[i].getName();
        }

        // can't invoke methods on array
        // TODO handle the exception if it doesn't exist
        int planetNumber = java.util.Arrays.asList(planetNames).indexOf(str);
        if (planetNumber == -1) {
            return 0;
        }
        else {
            return planetNumber;
        }
    }


    public boolean isInSkillRange(int agri, int medi, int tech) {
        return 1 <= agri && agri <= 5 || 1 <= medi && medi <= 5 || 1 <= tech
            && tech <= 5;
    }
}
